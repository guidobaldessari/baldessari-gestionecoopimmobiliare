delimiter //
create trigger before_delete_affitto
before delete on affitti
for each row
begin
	delete from spese where id_affitto = OLD.id_affitto;
end //

delimiter ;

delimiter //
create trigger before_delete_locatario
before delete on locatario
for each row
begin
	delete from affitti where id_locatario = OLD.id_locatario;
end //

delimiter ;

delimiter //
create trigger before_delete_immobile
before delete on immobile
for each row
begin
	delete from affitti where id_immobile = OLD.id_immobile;
end //

delimiter ;



            