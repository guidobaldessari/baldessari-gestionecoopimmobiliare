create database GestioneCoopImmobiliare;
use GestioneCoopImmobiliare;
create table Immobili(
	id_immobile INT primary key auto_increment,
	tipo varchar(1) not null,
    city varchar(50) not null);
    
    
create table Locatari(
	id_locatario INT primary key auto_increment,
    nominativo varchar(50) not null);
    

create table Affitti(
	id_affitto INT primary key auto_increment,
	ratax INT not null default 0,
    durata varchar(50) not null default 0,
    id_immobile INT not null,
    id_locatario INT not null,
    constraint fk_idImmobile_immobili foreign key(id_immobile) references Immobili(id_immobile),
    constraint fk_idLocatario_locatari foreign key(id_locatario) references Locatari(id_locatario),
    check(ratax >= 0),
    check(durata >= 0)
    );
    
create table Spese(
	id_spesa INT primary key auto_increment,
	importo decimal(13,4) not null,
    data date,
    id_affitto INT not null,
    constraint fk_idAffitto_affitti foreign key(id_affitto) references Affitti(id_affitto),
    check(importo >= 0)
    );



