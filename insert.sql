insert into immobili(id_immobile, tipo, city)
	values 
    (1,"A","varese"),
	(2,"N","como"),
	(3,"U","milano"),
	(4,"A","varese"),
	(5,"A","milano"),
    (6,"U","varese"),
	(7,"A","como");
    

insert into locatari(id_locatario, nominativo)
	values 
    (1,"rossi"),
	(2,"verdi"),
	(3,"bianchi"),
	(4,"neri");
    

 insert into affitti(id_affitto, ratax,durata,id_immobile, id_locatario)
	values 
    (1,1,5,2,1),
	(2,3,6,3,1),
    (3,0,2,4,2),
    (4,2,2,5,2),
    (5,1,1,6,3),
    (6,1,2,7,4),
    (7,0,2,7,4);

insert into spese(id_spesa, importo, data, id_affitto)
	values 
    (1,3000,"20070401",1),
	(2,5000,"20070701",1),
	(3,1500,"20090214",5),
	(4,4500,"20080503",7),
    (5,875,"20090115",7);
    