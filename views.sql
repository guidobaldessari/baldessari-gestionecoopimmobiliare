create view view_locatari2anni as
	select locatari.nominativo 
	from locatari inner join affitti
		on locatari.id_locatario = affitti.id_locatario
	where affitti.durata > 2;
    
create view view_immobili_affittati as
	select immobili.city, count(*)
	from immobili inner join affitti
		on immobili.id_immobile = affitti.id_immobile
	group by immobili.city;

create view view_affitti_arretrati as
	select affitti.*
    from affitti
    where ratax >= 3;


create view view_spese_rossi as
	select spese.*
    from Affitti natural join (select * from locatari where nominativo = "rossi") as t1
				 natural join (select * from spese where data between "20070101" and "20071231") as t2;

create view view_spese_bianchi as
	select nominativo, importo, data
    from (select * from locatari where nominativo = "bianchi") as t1 natural join affitti 
			natural join (select * from spese where importo > 1000) as t2;


create view view_spese_city as
	select city, sum(importo)
    from (immobili natural join affitti) natural join spese
    group by city;


create view view_numero_immobili_citta as
	select city, tipo
    from immobili
    group by tipo;
